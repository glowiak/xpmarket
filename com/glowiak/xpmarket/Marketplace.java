package com.glowiak.xpmarket;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

import java.util.ArrayList;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.net.URL;

public class Marketplace extends JFrame implements Runnable
{
    public static final double MARKETPLACE_VERSION = 1.1;
    
    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;
    
    public static final String repo = "https://github.com/glowiak/xpm-repo/raw/master/";
    
    public static Marketplace me;
    
    public int BX;
    public int BY;
    
    public ArrayList<SoftwarePackage> software;
    
    public SoftwarePanel sp;
    
    private JButton B_UP;
    private JButton B_DOWN;
    
    public static void main(String[] args)
    {
        me = new Marketplace();
        new Thread(me).start();
    }
    
    @Override
    public void run()
    {
        BX = 0;
        BY = 0;
        
        software = new ArrayList<SoftwarePackage>();
        sp = new SoftwarePanel(this);
        
        MarketplaceData.createDirs();
        MarketplaceData.refreshDatabase();
        
        setTitle("Marketplace");
        setSize(WIDTH, HEIGHT);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        
        B_UP = new JButton("/\\");
        B_UP.setBounds(WIDTH - 55, 5, 50, 50);
        B_UP.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                if (BY < 0)
                {
                    BY += 15;
                }
            }
        });
        add(B_UP);
        
        B_DOWN = new JButton("\\/");
        B_DOWN.setBounds(WIDTH - 55, HEIGHT - 75, 50, 50);
        B_DOWN.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                BY -= 15;
            }
        });
        add(B_DOWN);
        
        while (true)
        {
            super.repaint();
            sp.repaint();
        
            try {
                Thread.sleep(5);
            } catch (InterruptedException ie)
            {
                ie.printStackTrace();
            }
        }
    }
}
class SoftwarePanel extends JPanel
{
    Marketplace mp;
    
    public SoftwarePanel(Marketplace w)
    {
        this.mp = w;
        this.setBackground(Color.white);
        this.setFocusable(true);
        this.setBounds(0, 0, Marketplace.WIDTH, Marketplace.HEIGHT);
        this.setLayout(null);
        
        w.add(this);
    }
    
    @Override
    public void paintComponent(Graphics g)
    {
        g.setColor(Color.white);
        g.fillRect(0, 0, WIDTH, HEIGHT);
        
        g.setColor(Color.black);
        g.drawString("Welcome to Marketplace!", 250, mp.BY + 25);
        g.drawString("We have found " + mp.software.size() + " program(s) for you.", 220, mp.BY + 50);
        
        for (int i = 0; i < mp.software.size(); i++)
        {
            mp.software.get(i).repaint();
        }
    }
}
class SoftwarePackage extends JPanel
{
    public static int bxx = 10;
    public static int byy = 65;
    
    public String name;
    public String id;
    public Image icon;
    public String version;
    public int al_version;
    public String publisher;
    public String developer;
    public String installType;
    public String installer;
    
    private JLabel l_logo;
    private JLabel l_text;
    private JButton b_install;
    private JButton b_info;
    
    public int x;
    public int y;
    
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        
        if (icon != null)
        {
            l_logo.setIcon(new ImageIcon(icon));
        } else
        {
            l_logo.setText("NOIMG");
        }
        l_text.setText(name);
        l_logo.setBounds(Marketplace.me.BX, Marketplace.me.BY, 64, 64);
        l_text.setBounds(Marketplace.me.BX, Marketplace.me.BY + 70, 150, 25);
        b_install.setBounds(Marketplace.me.BX, Marketplace.me.BY + 90, 120, 25);
        b_info.setBounds(Marketplace.me.BX, Marketplace.me.BY + 120, 120, 25);
    }
    public SoftwarePackage()
    {
        this.x = bxx;
        this.y = byy;
        
        this.setFocusable(true);
        this.setBounds(this.x, this.y, 150, 200);
        this.setLayout(null);
        
        bxx += 150;
        if (bxx >= Marketplace.WIDTH - 150)
        {
            bxx = 0;
            byy += 150;
        }
        
        l_logo = new JLabel();
        add(l_logo);
        
        l_text = new JLabel(name);
        add(l_text);
        
        b_install = new JButton("Install");
        b_install.addActionListener(e -> new SoftwareInstaller(this));
        add(b_install);
        
        b_info = new JButton("Info");
        b_info.addActionListener(e -> new InfoViewer(this));
        add(b_info);
        
        Marketplace.me.sp.add(this);
    }
    public static SoftwarePackage loadFromFile(String path)
    {
        SoftwarePackage w = new SoftwarePackage();
        
        try {
            FileReader fr = new FileReader(path);
            BufferedReader br = new BufferedReader(fr);
            
            String s = null;
            while ((s = br.readLine()) != null)
            {
                if (s.contains("Name="))
                {
                    w.name = s.replace("Name=", "");
                } else
                if (s.contains("Id="))
                {
                    w.id = s.replace("Id=", "");
                } else
                if (s.contains("Icon="))
                {
                    String url = s.replace("Icon=", "");
                    boolean b = DownloadFile.fetch(url, MarketplaceData.D_ICONS + "/" + w.id + ".png");
                    if (b)
                    {
                        w.icon = new ImageIcon(MarketplaceData.D_ICONS + "/" + w.id + ".png").getImage().getScaledInstance(64, 64, Image.SCALE_DEFAULT);
                    } else
                    {
                        w.icon = null;
                    }
                } else
                if (s.contains("Version="))
                {
                    w.version = s.replace("Version=", "");
                } else
                if (s.contains("Alversion="))
                {
                    w.al_version = Integer.parseInt(s.replace("Alversion=", ""));
                } else
                if (s.contains("Publisher="))
                {
                    w.publisher = s.replace("Publisher=", "");
                } else
                if (s.contains("Developer="))
                {
                    w.developer = s.replace("Developer=", "");
                } else
                if (s.contains("InstallType="))
                {
                    w.installType = s.replace("InstallType=", "");
                } else
                if (s.contains("Installer="))
                {
                    w.installer = s.replace("Installer=", "");
                }
            }
            
            br.close();
            fr.close();
        } catch (IOException ioe)
        {
            ioe.printStackTrace();
            return null;
        }
        
        return w;
    }
}
class MarketplaceData
{
    public static final String U_HOME = System.getProperty("user.home");
    public static final String D_HOME = U_HOME + "/" + "marketplace";
    public static final String D_REPO = D_HOME + "/" + "repo";
    public static final String D_DOWNLOADS = D_HOME + "/" + "downloads";
    public static final String D_ICONS = D_HOME + "/" + "icons";
    public static final String D_DB = D_HOME + "/" + "/db";
    
    public static void createDirs()
    {
        new File(D_HOME).mkdirs();
        new File(D_REPO).mkdirs();
        new File(D_DOWNLOADS).mkdirs();
        new File(D_ICONS).mkdirs();
        new File(D_DB).mkdirs();
    }
    public static boolean refreshDatabase()
    {
        boolean w = true;
        
        rmdir(new File(D_DB));
        new File(D_DB).mkdirs();
        
        System.out.println("UPDATE INDEX");
        boolean q = DownloadFile.fetch(Marketplace.repo + "list.txt", D_DB + "/index");
        if (!q)
        {
            w = false;
        }
        
        ArrayList<String> names = new ArrayList<String>();
        
        try {
            FileReader fr = new FileReader(D_DB + "/index");
            BufferedReader br = new BufferedReader(fr);
            
            String s = null;
            while ((s = br.readLine()) != null)
            {
                names.add(s);
            }
            
            br.close();
            fr.close();
        } catch (IOException ioe)
        {
            ioe.printStackTrace();
            w = false;
        }
        
        for (int i = 0; i < names.size(); i++)
        {
            System.out.println("DOWNLOAD " + names.get(i));
            q = DownloadFile.fetch(Marketplace.repo + names.get(i) + ".prg", D_DB + "/" + names.get(i));
            if (!q)
            {
                w = false;
            }
            System.out.println("LOAD " + names.get(i));
            Marketplace.me.software.add(SoftwarePackage.loadFromFile(D_DB + "/" + names.get(i)));
        }
        
        return w;
    }
    public static boolean rmdir(File dir) { // based on https://www.baeldung.com/java-delete-directory
        System.out.println("REMOVE " + dir.getPath());
        File[] allContents = dir.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                rmdir(file);
            }
        }
        return dir.delete();
    }

}
class DownloadFile
{
    public static boolean fetch(String url, String dst)
    {
        try {
            File ie = new File(dst);
            if (!ie.exists()) {
                URL u1 = new URL(url);
                Path u2 = Paths.get(dst);
                InputStream is = u1.openStream();
                Files.copy(is, u2, StandardCopyOption.REPLACE_EXISTING);
            }
            
            return true;
        } catch (Exception ioe)
        {
            ioe.printStackTrace();
            return false;
        }
    }
}
class InfoViewer extends JFrame
{
    private SoftwarePackage sp;
    
    private JButton b_done;
    
    public InfoViewer(SoftwarePackage sp)
    {
        this.sp = sp;
        
        setTitle("Package Info");
        setSize(300, 300);
        setVisible(true);
        setLayout(null);
        
        b_done = new JButton("Done");
        b_done.setBounds(25, 250, 250, 25);
        b_done.addActionListener(e -> dispose());
        add(b_done);
    }
    
    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        
        g.setColor(Color.black);
        g.drawString("Name: " + sp.name, 25, 45);
        g.drawString("Id: " + sp.id, 25, 60);
        g.drawString("Version: " + sp.version, 25, 75);
        g.drawString("Publisher: " + sp.publisher, 25, 90);
        g.drawString("Developer: " + sp.developer, 25, 105);
    }
}
class SoftwareInstaller
{
    private SoftwarePackage sp;
    
    public SoftwareInstaller(SoftwarePackage sp)
    {
        this.sp = sp;
        
        System.out.println("INSTALLING: " + sp.name);
        if (sp.installType.equals("DownloadAndRun"))
        {
            System.out.println("TRY DOWNLOAD " + sp.installer);
            boolean b = DownloadFile.fetch(sp.installer, MarketplaceData.D_DOWNLOADS + "/" + sp.id + "-installer.exe");
            if (!b)
            {
                System.out.println("ERROR DOWNLOADING!");
                JOptionPane.showMessageDialog(null, "Error installing package: could not download the installer.");
                return;
            }
            try {
                System.out.println("TRY RUN");
                Process p = Runtime.getRuntime().exec(MarketplaceData.D_DOWNLOADS + "/" + sp.id + "-installer.exe");
            
                BufferedReader br_log = new BufferedReader(new InputStreamReader(p.getInputStream()));
                BufferedReader br_err = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            
                String s = null;
                while ((s = br_log.readLine()) != null)
                {
                    System.out.println(s);
                }
                while ((s = br_err.readLine()) != null)
                {
                    System.out.println(s);
                }
            
                new File(MarketplaceData.D_DB + "/" + sp.id).createNewFile();
            
                JOptionPane.showMessageDialog(null, "Package installation completed.");
            
                br_log.close();
                br_err.close();
            } catch (IOException ioe)
            {
                ioe.printStackTrace();
            }
        }
    }
}
class InstallationManager
{
    public static boolean isInstalled(SoftwarePackage sp)
    {
        boolean w = false;
        
        if (new File(MarketplaceData.D_DB + "/" + sp.id).exists())
        {
            w = true;
        }
        
        return w;
    }
}
